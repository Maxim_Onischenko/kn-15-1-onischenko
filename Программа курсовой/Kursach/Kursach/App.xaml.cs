﻿using Kursach;
using Kursach.Model;
using Kursach.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Kursach
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        App()
        {
            StartVM StartV = new StartVM();
            StartView startView = new StartView();
            startView.DataContext = StartV;
            startView.Closing += StartView_Closing;
            startView.Show();
        }

        private void StartView_Closing(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
