﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach.Model
{
    public class Project
    {
        public string ProjectName { get; set; }
        public double Time { get; set; }
        public int WorkHours { get; set; }
        public double Сost { get; set; }
        public string Customer { get; set; }
        public int Validation { get; set; }
        public Project[] ProjectArray { get; set; }


    }
}
