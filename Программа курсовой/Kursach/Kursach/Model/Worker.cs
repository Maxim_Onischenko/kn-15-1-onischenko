﻿using Kursach.Model;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace Kursach
{

    public class Worker
    {
        public Worker()
        {
            WorkerList = new List<Worker>();
        }

        public List<Worker> WorkerList { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int SalaryAccount { get; set; }
        public double Seniority { get; set; }
        public Post WorkerPost { get; set; }

        public void Save(List<Worker> worker)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Worker>));
            using (FileStream save = new FileStream("worker.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(save, worker);
            }
        }

        public List<Worker> Load()
        {
            using (FileStream load = new FileStream("worker.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<Worker>));
                return (List<Worker>)formatter.Deserialize(load);
            }
        }

        public void Add()
        {
            WorkerList.Add(new Worker { });
        }

        public void Delete(Worker worker)
        {
            if (worker != null)
            {
                WorkerList.Remove(worker);
            }
            else if (worker == null) MessageBox.Show("Вкажіть робітника");
        }
    }
}
