﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kursach.Model
{
    public class Post
    {
        public List<PostToChange> PostList { get; set; }

        public Post()
        {
            PostList = new List<PostToChange>();
        }

        public void Save(List<PostToChange> things)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<PostToChange>));
            using (FileStream fs = new FileStream("posts.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, things);
            }
        }

        public List<PostToChange> Load()
        {
            using (FileStream fs = new FileStream("posts.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<PostToChange>));
                return (List<PostToChange>)formatter.Deserialize(fs);
            }
        }

        public void Add(PostToChange post)
        {
            PostList.Add(post);
        }

        public void Add(List<PostToChange> posts)
        {
            PostList.AddRange(posts);
        }

        public void Delete(PostToChange post)
        {
            if (PostList.Contains(post))
                PostList.Remove(post);
            else
                throw new Exception("Не знайдено.");
        }

    }
}
