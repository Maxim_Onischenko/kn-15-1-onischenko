﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach.Model
{
    [Serializable]
    public class PostToChange
    {
        public PostToChange()
        {

        }

        public PostToChange (string name, string responsobility, string hours, string salary)
        {
            Name = name;
            Responsibility = responsobility;
            Hours = hours;
            Salary = salary;
        }


        public string Name { get; set; }
        public string Responsibility { get; set; }
        public string Hours { get; set; }
        public string Salary { get; set; }
    }
}
