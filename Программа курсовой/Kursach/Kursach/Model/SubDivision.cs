﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace Kursach.Model
{
    public class SubDivision
    {
        public string DivisionName { get; set; }
        public string BossName { get; set; }
        public double Award { get; set; }
        public string SDWorker { get; set; }

      public SubDivision()
        {
            SubDivisionList = new List<SubDivision>();
        }

        public List<SubDivision> SubDivisionList { get; set; }


        public void Save(List<SubDivision> subdivision)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<SubDivision>));
            using (FileStream save = new FileStream("SubDivision.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(save, subdivision);
            }
        }

        public List<SubDivision> Load()
        {
            using (FileStream load = new FileStream("SubDivision.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<SubDivision>));
                return (List<SubDivision>)formatter.Deserialize(load);
            }
        }

        public void Add()
        {
            SubDivisionList.Add(new SubDivision { });
        }

        public void Delete(SubDivision subdivision)
        {
            if (subdivision != null)
            {
                SubDivisionList.Remove(subdivision);
            }
            else if (subdivision == null) MessageBox.Show("Вкажіть підрозділ");
        }

    }
}
