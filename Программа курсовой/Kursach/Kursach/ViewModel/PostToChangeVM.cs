﻿using Kursach.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach.ViewModel
{
    public class PostToChangeVM
    {
        public class ThingToBuyViewModel : VMBase
        {
            PostToChange _post;

            public ThingToBuyViewModel(PostToChange post)
            {
                _post = post;
            }

            public string Name
            {
                get { return _post.Name; }
                set
                {
                    _post.Name = value;
                    OnPropertyChanged("Name");
                }
            }

            public string Salary
            {
                get { return _post.Salary; }
                set
                {
                    _post.Salary = value;
                    OnPropertyChanged("Name");
                }
            }

            public string Responsobility
            {
                get { return _post.Responsibility; }
                set
                {
                    _post.Responsibility = value;
                    OnPropertyChanged("Name");
                }
            }

            public string Hours
            {
                get { return _post.Hours; }
                set
                {
                    _post.Hours = value;
                    OnPropertyChanged("Amount");
                }
            }
        }
    }
}
