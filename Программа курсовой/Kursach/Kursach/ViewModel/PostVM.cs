﻿using Kursach.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Kursach.ViewModel
{
    public class PostVM : VMBase
    {
        public PostVM(Post post)
        {
            _post = post;
            PostList = _post.PostList;
            Collection = new ObservableCollection<PostToChange>(PostList);
            SaveCommand = new Command(arg => Save());
            AddCommand = new Command(arg => Add());
            OpenCommand = new Command(arg => Open());
            DeleteCommand = new Command(arg => Delete());
            CloseCommand = new Command(arg => Close?.Invoke());
        }

        public ICommand SaveCommand { get; set; }
        public ICommand OpenCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public event Action Close;

        public ObservableCollection<PostToChange> _collection;

        public ObservableCollection<PostToChange> Collection
        {
            get { return _collection; }
            set { _collection = value; OnPropertyChanged("Collection"); }
        }

        private Post _post;

        private List<PostToChange> _postList;

        public List<PostToChange> PostList
        {
            get
            {
                return _postList;
            }
            set
            {
                _postList = value;
                OnPropertyChanged(nameof(PostList));
            }
        }

        private PostToChange _selected;

        public PostToChange Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                OnPropertyChanged(nameof(Selected));
            }
        }

        public void Add()
        {
            var addViewModel = new AddPostVM(_post);
            var addView = new AddPostView(addViewModel);
            addViewModel.Generated += (data => { _post.Add(data); });
            addView.Closed += RefreshData;
            addView.ShowDialog();
        }

        private void RefreshData(object sender, EventArgs e)
        {
            PostList = _post.PostList;
            Collection = new ObservableCollection<PostToChange>(PostList);
            OnPropertyChanged(nameof(Collection));
        }

        private void Save()
        {
            try
            {
                _post.Save(PostList);
            }
            catch (Exception)
            {
                MessageBox.Show("Помилка");
            }
        }

        private void Open()
        {
            try
            {
                _post.PostList = _post.Load();
                this.PostList = _post.PostList;
                Collection = new ObservableCollection<PostToChange>(PostList);
                OnPropertyChanged(nameof(Collection));
            }
            catch (Exception)
            {
                MessageBox.Show("Помилка");
            }
        }

        private void Delete()
        {
            try
            {
                if (Selected != null)
                {
                    _post.Delete(Selected);
                    RefreshData(null, null);
                }
                else
                    throw new Exception("Ничего не выбрано");
            }
            catch (Exception)
            {
                MessageBox.Show("Помилка");
            }
        }
    }
}
