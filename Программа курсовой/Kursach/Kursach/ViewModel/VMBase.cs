﻿using System.ComponentModel;

namespace Kursach.ViewModel
{
    public abstract class VMBase : INotifyPropertyChanged
    {
            public event PropertyChangedEventHandler PropertyChanged;// = delegate { };

            protected void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
    }
}
