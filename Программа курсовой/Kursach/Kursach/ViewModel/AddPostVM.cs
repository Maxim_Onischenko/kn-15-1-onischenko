﻿using Kursach.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Kursach.Model
{
    public class AddPostVM : VMBase
    {

        public string Name { get; set; }
        public string Responsibility { get; set; }
        public string Hours { get; set; }
        public string Salary { get; set; }

        Post _postList;

        public AddPostVM(Post postList)
        {
            _postList = postList;
            AddCommand = new Command(arg => Add());
        }

        public ICommand AddCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public event Action<PostToChange> Generated;

        void Add()
        {
            try
            {
                Generated?.Invoke(new PostToChange(this.Name, this.Hours, this.Responsibility, this.Salary));
                if (CloseCommand != null)
                    CloseCommand.Execute(null);
            }
            catch (Exception)
            {
                MessageBox.Show("Помилка!");
            }
        }
    }
}
