﻿using Kursach.Model;
using Kursach.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Kursach.ViewModel
{
    public class SubDivisionVM :VMBase
    {
        
        public SubDivisionVM(SubDivision subDivision)
        {
            _subDivision = subDivision;
            SubDivisionList = _subDivision.SubDivisionList;

            SubDivisionCollection = new ObservableCollection<SubDivision>(SubDivisionList);

            AddCommand = new Command(arg => Add());
            OpenCommand = new Command(arg => Open());
            DeleteCommand = new Command(arg => Delete());
            SaveCommand = new Command(arg => Save());
        }

        public ICommand AddCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand OpenCommand { get; set; }
        public ICommand SaveCommand { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int SalaryAccount { get; set; }
        public double Seniority { get; set; }


        private void Open()
        {
            try
            {
                _subDivision.SubDivisionList = _subDivision.Load();
                SubDivisionList = _subDivision.SubDivisionList;
                SubDivisionCollection = new ObservableCollection<SubDivision>(SubDivisionList);
                OnPropertyChanged(nameof(SubDivisionCollection));
            }
            catch (Exception)
            {
                MessageBox.Show("Не вдалось завантажити файл");
            }
        }

        private void Save()
        {
            try
            {
                _subDivision.Save(SubDivisionList);
            }
            catch (Exception)
            {
                MessageBox.Show("Файл не вдалось зберегти");
            }
        }

        private void RefreshData(object sender, EventArgs e)
        {
            SubDivisionList = _subDivision.SubDivisionList;
            SubDivisionCollection = new ObservableCollection<SubDivision>(SubDivisionList);
            OnPropertyChanged(nameof(SubDivisionCollection));
        }

        private void Add()
        {
            if (_subDivision != null)
                _subDivision.Add();
            RefreshData(null, null);
        }

        private void Delete()
        {
            try
            {
                if (Selected != null)
                {

                    _subDivision.Delete(Selected);
                    RefreshData(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Не вдалось видалити робітника");
            }

        }

        private SubDivision _selected;

        public SubDivision Selected
        {
            get
            {
                return _selected;
            }

            set
            {
                _selected = value;
                OnPropertyChanged("Selected");
            }
        }

        SubDivision _subDivision;

        public SubDivision subDivision
        {
            get { return _subDivision; }
            set
            {
                _subDivision = value;
                OnPropertyChanged(nameof(subDivision));
            }
        }

        private List<SubDivision> _subDivisionList;

        public List<SubDivision> SubDivisionList
        {
            get { return _subDivisionList; }

            set
            {
                _subDivisionList = value;
                OnPropertyChanged(nameof(SubDivisionList));
            }
        }


        private ObservableCollection<SubDivision> _subDivisionCollection { get; set; }

        public ObservableCollection<SubDivision> SubDivisionCollection
        {
            get
            { return _subDivisionCollection; }

            set
            {
                _subDivisionCollection = value;
                OnPropertyChanged(nameof(SubDivisionCollection));
            }
        }



        public string DivisionName
        {
            get { return _subDivision.DivisionName; }

            set
            {
                _subDivision.DivisionName = value;
                OnPropertyChanged("DivisionName");
            }
        }


        public string BossName
        {
            get { return _subDivision.BossName; }

            set
            {
                _subDivision.BossName = value;
                OnPropertyChanged("BossName");
            }
        }


        public double Award
        {
            get { return _subDivision.Award; }
            set
            {
                _subDivision.Award = value;
                OnPropertyChanged("Award");
            }
        }

        public string SDWorker
        {
            get { return _subDivision.SDWorker; }
            set
            {
                _subDivision.SDWorker = value;
                OnPropertyChanged("SDWorker");
            }
        }


    }
}
