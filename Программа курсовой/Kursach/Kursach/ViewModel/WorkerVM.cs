﻿using Kursach.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Kursach.ViewModel
{
    public class WorkerVM : VMBase
    {

        Project _project;
         
        public Project project
        {
            get { return _project; }
            set
            {
                _project = value;
                OnPropertyChanged(nameof(project));
            }
        }

        private Worker _worker;

        public Worker worker
        {
            get { return _worker; }
            set
            {
                _worker = value;
                OnPropertyChanged(nameof(worker));
            }
        }

        public WorkerVM(Worker worker, Project project)
        {
                _worker = worker;
                WorkerList = _worker.WorkerList;
                _project = project;
                ProjectArray = _project.ProjectArray;

            WorkerPost = new Post() {};

            WorkersCollection = new ObservableCollection<Worker>(WorkerList);


            ProjectArray = new Project[]
               {
                    new Project { ProjectName = "Personal managment", Time = 4.5, WorkHours = 58},
                    new Project { ProjectName = "Calculator for Website", Time = 3, WorkHours = 38},
                    new Project { ProjectName = "Algorithm manager", Time = 2.5, WorkHours = 53},
                    new Project { ProjectName = "VPN App", Time = 2.5, WorkHours = 53, Сost = 4644.2},
                    new Project { ProjectName = "Big Data", Time = 3, WorkHours = 38, Сost = 4754}
                };

                OpenCommand = new Command(arg => Open());
                AddCommand = new Command(arg => Add());
                DeleteCommand = new Command(arg => Delete());
                SaveCommand = new Command(arg => Save());
                WorkerProjectsCommand = new Command(arg => ShowProjects());
                AllProjectsCommand = new Command(arg => ShowAllProjects());
        }

        public ICommand DeleteCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand SaveCommand { get; set; }
        public ICommand WorkerProjectsCommand { get; set; }
        public ICommand AllProjectsCommand { get; set; }
        public ICommand OpenCommand { get; set; }


        private void ShowAllProjects()
        {
            ProjectArray = new Project[]
            {
                new Project { ProjectName = "Personal managment", Time = 4.5, WorkHours = 58},
                new Project { ProjectName = "Calculator for Website", Time = 3, WorkHours = 38},
                new Project { ProjectName = "Algorithm manager", Time = 2.5, WorkHours = 53},
                new Project { ProjectName = "VPN App", Time = 2.5, WorkHours = 53},
                new Project { ProjectName = "Big Data", Time = 3, WorkHours = 38}
            };

        }


        private void ShowProjects()
        {
            try
            {
                if (Selected.SalaryAccount == 1)
                {
                    ProjectArray = new Project[]
                    {
                        new Project { ProjectName = "VPN App", Time = 2.5, WorkHours = 53},
                        new Project { ProjectName = "Calculator for Website", Time = 3, WorkHours = 38},
                        new Project { ProjectName = "Algorithm manager", Time = 2.5, WorkHours = 53}
                    };
                }

                else if (Selected.SalaryAccount == 2)
                {
                    ProjectArray = new Project[]
                    {
                        new Project { ProjectName = "Personal managment", Time = 4.5, WorkHours = 58},
                        new Project { ProjectName = "Calculator for Website", Time = 3, WorkHours = 38}
                    };
                }

                else if (Selected.SalaryAccount == 3)
                {
                    ProjectArray = new Project[]
                    {
                    new Project { ProjectName = "VPN App", Time = 2.5, WorkHours = 53},
                    new Project { ProjectName = "Personal managment", Time = 4.5, WorkHours = 58},
                    };
                }
                else
                {
                    ProjectArray = new Project[]
                   {
                    new Project() { }
                   };

                    MessageBox.Show("Робітник не має проектів");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Вкажіть робітника");
            }

            OnPropertyChanged(nameof(ProjectArray));
        }

        private void Open()
        {
            try
            {
                _worker.WorkerList = _worker.Load();
                WorkerList = _worker.WorkerList;
                WorkersCollection = new ObservableCollection<Worker>(WorkerList);
                OnPropertyChanged(nameof(WorkersCollection));
            }
            catch (Exception)
            {
                MessageBox.Show("Не вдалось завантажити файл");
            }
        }

        private void Save()
        {
            try
            {
                _worker.Save(WorkerList);
            }
            catch (Exception)
            {
                MessageBox.Show("Файл не вдалось зберегти");
            }
        }

        private void RefreshData(object sender, EventArgs e)
        {
            WorkerList = _worker.WorkerList;
            WorkersCollection = new ObservableCollection<Worker>(WorkerList);
            OnPropertyChanged(nameof(WorkersCollection));
        }

        private void Add()
        {
                if (_worker != null)
                _worker.Add();
                RefreshData(null, null);
        }

        private void Delete()
        {
            try
            {
                if (Selected != null)
                {

                    _worker.Delete(Selected);
                    RefreshData(null, null);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Не вдалось видалити робітника");
            }
            
        }

        void Search()
        {
            if (string.IsNullOrEmpty(SearchElement))
            {
                WorkersCollection.Clear();
                WorkersCollection = new ObservableCollection<Worker>(WorkerList);
            }
            else
            {
                string element = _searchElement.ToUpper();
                WorkersCollection.Clear();
                foreach (var item in WorkerList)
                {
                    if (item.FirstName.ToString().ToUpper().Contains(element)) WorkersCollection.Add(item);
                    else if (item.SalaryAccount.ToString().ToUpper().Contains(element)) WorkersCollection.Add(item);
                    else if (item.SecondName.ToString().ToUpper().Contains(element)) WorkersCollection.Add(item);
                    else if (item.Seniority.ToString().ToUpper().Contains(element)) WorkersCollection.Add(item);
                }
            }
        }


        private Worker _selected;

        public Worker Selected
        {
            get
            {
                return _selected;
            }

            set
            {
                _selected = value;
                OnPropertyChanged("Selected");
            }
        }

        private List<Worker> _workerList { get; set; }

        public List<Worker> WorkerList
        {
            get { return _workerList; }

            set
            {
                _workerList = value;
                OnPropertyChanged(nameof(WorkerList));
            }
        }

        private ObservableCollection<Worker> _workersCollection;

        public ObservableCollection<Worker> WorkersCollection
        {
            get
            {
                return _workersCollection;
            }
            set
            {
                _workersCollection = value;
                OnPropertyChanged(nameof(WorkersCollection));
            }
        }


        private Project[] _projectArray;

        public Project[] ProjectArray
        {
            get
            {
                return _projectArray;
            }
            set
            {
                _projectArray = value;
                OnPropertyChanged(nameof(ProjectArray));
            }
        }

        private Post _workerPost { get; set; }

        public Post WorkerPost
        {
            get { return _workerPost; }

            set
            {
                _workerPost = value;
                OnPropertyChanged("WorkerPost");  
            }
        }

        private string _searchElement;

        public string SearchElement
        {
            get { return _searchElement; }
            set
            {
                _searchElement = value;
                Search();
            }
        }

        private string _name;

        public string Name
        {
            get { return _name; }

            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        private string _hours;

        public string Hours
        {
            get { return _hours; }

            set
            {
                _hours = value;
                OnPropertyChanged("Hours");
            }
        }


        public string FirstName
        {
            get { return _worker.FirstName; }

            set
            {
                _worker.FirstName = value;
                OnPropertyChanged("FirstName");
            }
        }


        public string SecondName
        {
            get { return _worker.SecondName; }

            set
            {
                _worker.SecondName = value;
                OnPropertyChanged("SecondName");
            }
        }


        public int SalaryAccount
        {
            get { return _worker.SalaryAccount; }

            set
            {
                _worker.SalaryAccount = value;
                OnPropertyChanged("SalaryAccount");
            }
        }


        public double Seniority
        {
            get { return _worker.Seniority; }
            set
            {
                _worker.Seniority = value;
                OnPropertyChanged("Seniority");
            }
        }

    }
}
