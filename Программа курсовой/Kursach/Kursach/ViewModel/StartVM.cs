﻿using Kursach.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kursach.ViewModel
{
    public class StartVM : VMBase
    {
        Worker worker;
        Project project;
        SubDivision subDivision;
        Post postList;
        PostView postView;
        PostVM postVM;

        public StartVM()
        {
            ExitCommand = new Command(arg => Exit());
            SubDivisionViewCommand = new Command(arg => SubDivisionStart());
            WorkerViewCommand = new Command(arg => WorkerStart());
            PostViewCommand = new Command(arg => PostStart());
        }

        public ICommand WorkerViewCommand { get; set; }
        public ICommand ExitCommand { get; set; }
        public ICommand SubDivisionViewCommand { get; set; }
        public ICommand PostViewCommand { get; set; }

        private void Exit()
        {
            Environment.Exit(0);
        }

        private void WorkerStart ()
        {
            worker = new Worker();
            project = new Project();
            WorkerVM WorkerVMStart = new WorkerVM(worker, project);
            MainWindow mainWindow = new MainWindow();
            mainWindow.DataContext = WorkerVMStart;
            mainWindow.ShowDialog();
        }

        private void SubDivisionStart()
        {
            subDivision = new SubDivision();
            SubDivisionVM SubDivisionVMStart = new SubDivisionVM(subDivision);
            Division division = new Division();
            division.DataContext = SubDivisionVMStart;
            division.ShowDialog();
        }

        private void PostStart()
        {
            postList = new Post();
            postVM = new PostVM(postList);
            postVM.Close += this.CloseApp;
            PostView postView = new PostView();
            postView.DataContext = postVM;
            postView.Show();
        }

        public void CloseApp()
        {
            postView.Close();
        }
    }
}
