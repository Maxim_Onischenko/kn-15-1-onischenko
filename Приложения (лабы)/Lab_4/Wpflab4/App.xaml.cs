﻿using Wpflab4.ViewModel;
using Wpflab4.View;
using Wpflab4.Model;
using System.Windows;
using System.Collections.Generic;

namespace Wpflab4
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        Things thingsList;
        MainWindow view;
        MainWindowViewModel viewModel;

        public App()
        {
            thingsList = new Things();
            view = new MainWindow();
            viewModel = new MainWindowViewModel(thingsList);
            viewModel.Close += this.CloseApp;
            view.DataContext = viewModel;
            view.Show();
        }

        public void CloseApp()
        {
            view.Close();
        }
    }
}
