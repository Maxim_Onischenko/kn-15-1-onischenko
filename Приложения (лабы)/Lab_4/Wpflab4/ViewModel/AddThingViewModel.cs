﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Wpflab4.Model;

namespace Wpflab4.ViewModel
{
    public class AddThingViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public string Amount { get; set; }
        Things _thingsList;

        public AddThingViewModel(Things thingsList)
        {
            _thingsList = thingsList;
            AddCommand = new Command(arg => Add());
        }

        public ICommand AddCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public event Action<ThingToBuy> Generated;

        void Add()
        {
            try
            {
                Generated?.Invoke(new ThingToBuy(this.Name, Convert.ToInt32(this.Amount)));
                if (CloseCommand != null)
                    CloseCommand.Execute(null);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка!");
            }
        }
    }
}
