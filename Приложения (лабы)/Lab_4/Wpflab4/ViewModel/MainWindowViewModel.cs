﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Wpflab4.Model;
using Wpflab4.View;

namespace Wpflab4.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel(Things things)
        {
            _things = things;
            ThingsList = _things.ThingsList;
            Collection = new ObservableCollection<ThingToBuy>(ThingsList);
            SaveCommand = new Command(arg => Save());
            AddCommand = new Command(arg => Add());
            OpenCommand = new Command(arg => Open());
            DeleteCommand = new Command(arg => Delete());
            CloseCommand = new Command(arg => Close?.Invoke());
            AboutCommand = new Command(arg => MessageBox.Show("Лабораторная работа.\nПриложение \"Список покупок\""));
        }   

        public ICommand SaveCommand { get; set; }
        public ICommand OpenCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand AboutCommand { get; set; }
        public event Action Close;

        private string _searchQuery;

        public string SearchQuery
        {
            get { return _searchQuery; }
            set
            {
                _searchQuery = value;
                Search();
            }
        }

        public ObservableCollection<ThingToBuy> _collection;

        public ObservableCollection<ThingToBuy> Collection
        {
            get { return _collection; }
            set { _collection = value; OnPropertyChanged("Collection"); }
        }

        private string _status;

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        private Things _things;

        private List<ThingToBuy> _thingsList;

        public List<ThingToBuy> ThingsList
        {
            get
            {
                return _thingsList;
            }
            set
            {
                _thingsList = value;
                OnPropertyChanged(nameof(ThingsList));
            }
        }

        private ThingToBuy _selected;

        public ThingToBuy Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                OnPropertyChanged(nameof(Selected));
            }
        }

        public void Add()
        {
            var addViewModel = new AddThingViewModel(_things);
            var addView = new AddThingView(addViewModel);
            Status = "Не добавлено";
            addViewModel.Generated += (data => { Status = "Добавлено"; _things.Add(data); });
            addView.Closed += RefreshData;
            addView.ShowDialog();
        }

        private void RefreshData(object sender, EventArgs e)
        {
            ThingsList = _things.ThingsList;
            Collection = new ObservableCollection<ThingToBuy>(ThingsList);
            OnPropertyChanged(nameof(Collection));
        }

        private void Save()
        {
            try
            {
                _things.Save(ThingsList);
                Status = "Сохранено";
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }
        }

        private void Open()
        {
            try
            {
                _things.ThingsList = _things.Load();
                this.ThingsList = _things.ThingsList;
                Collection = new ObservableCollection<ThingToBuy>(ThingsList);
                OnPropertyChanged(nameof(Collection));
                Status = "Загружено";
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }
        }

        private void Delete()
        {
            try
            {
                if (Selected != null)
                {
                    _things.Delete(Selected);
                    RefreshData(null, null);
                }
                else
                    throw new Exception("Ничего не выбрано");
                Status = "Удалено";
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }
        }

        void Search()
        {
            if (string.IsNullOrEmpty(SearchQuery))
            {
                Collection.Clear();
                Collection = new ObservableCollection<ThingToBuy>(ThingsList);
            }
            else
            {
                string query = _searchQuery.ToUpper();
                Collection.Clear();
                foreach (var item in ThingsList)
                {
                    if (item.Name.ToUpper().Contains(query))
                        Collection.Add(item);
                }
            }
        }
    }
}
