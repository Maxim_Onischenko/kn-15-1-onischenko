﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpflab4.Model;

namespace Wpflab4.ViewModel
{
    public class ThingToBuyViewModel : ViewModelBase
    {
        ThingToBuy _thing;

        public ThingToBuyViewModel(ThingToBuy thing)
        {
            _thing = thing;
        }

        public string Name
        {
            get { return _thing.Name; }
            set
            {
                _thing.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public int Amount
        {
            get { return _thing.Amount; }
            set
            {
                _thing.Amount = value;
                OnPropertyChanged("Amount");
            }
        }
    }
}
