﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Wpflab4.Model
{
    public class Things
    {
        public List<ThingToBuy> ThingsList { get; set; }

        public Things()
        {
            ThingsList = new List<ThingToBuy>();
        }
        public List<ThingToBuy> GetAll()
        {
            return ThingsList;
        }
        
        public void Save(List<ThingToBuy> things)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<ThingToBuy>));
            using (FileStream fs = new FileStream("things.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, things);
            }
        }

        public List<ThingToBuy> Load()
        {
            using (FileStream fs = new FileStream("things.xml", FileMode.OpenOrCreate))
            {
                XmlSerializer formatter = new XmlSerializer(typeof(List<ThingToBuy>));
                return (List<ThingToBuy>)formatter.Deserialize(fs);
            }
        }

        public void Add(ThingToBuy thing)
        {
            ThingsList.Add(thing);
        }

        public void Add(List<ThingToBuy> things)
        {
            ThingsList.AddRange(things);
        }

        public void Delete(ThingToBuy thing)
        {
            if (ThingsList.Contains(thing))
                ThingsList.Remove(thing);
            else
                throw new Exception("Not found.");
        }
    }
}
