﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wpflab4.Model
{
    [Serializable]
    public class ThingToBuy
    {
        public ThingToBuy()
        {

        }
        public ThingToBuy(string name, int amount)
        {
            Name = name;
            Amount = amount;
        }
        public string Name { get; set; }
        public int Amount { get; set; }
    }
}
