﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Wpflab4.ViewModel;

namespace Wpflab4.View
{
    /// <summary>
    /// Логика взаимодействия для AddThingView.xaml
    /// </summary>
    public partial class AddThingView : Window
    {
        public AddThingView(AddThingViewModel addThingViewModel)
        {
            InitializeComponent();
            this.DataContext = addThingViewModel;
            if (addThingViewModel.CloseCommand == null)
                addThingViewModel.CloseCommand = new Command((arg) => this.Close());
        }
    }
}
