﻿using System;
using System.Windows;

namespace Laba2OOP_home
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        public void button_Click(object sender, RoutedEventArgs e)
        {
            PictureView view = new PictureView(comboBox.SelectedIndex);
            view.ShowDialog();
        }
    }
}
