﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Laba2OOP_home
{
    public partial class PictureView : Window
    {
        public PictureView()
        {

        }

        public PictureView(int i)
        {
            InitializeComponent();
            PictureViewer(i);
        }

        private void PictureViewer(int value)
        {
            string number = value.ToString();
            string where = String.Format("Resources/{0}.png",number);
            myPicture.Source = new BitmapImage(new Uri(where, UriKind.Relative));

        }
    }
}
