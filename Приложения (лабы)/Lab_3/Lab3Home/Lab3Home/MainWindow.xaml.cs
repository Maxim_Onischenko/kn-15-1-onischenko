﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace FirstWpfApp
{
    public partial class MainWindow : Window
    {
        string lefеElement = ""; 
        string operation = ""; 
        string rightElement = "";

        public MainWindow()
        {
            InitializeComponent();
            foreach (UIElement c in GridRoot.Children)
            {
                if (c is Button)
                {
                    ((Button)c).Click += Button_Click;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string s = (string)((Button)e.OriginalSource).Content;
            textBlock.Text += s;
            int num;
            bool result = Int32.TryParse(s, out num);
            if (result == true)
            {
                if (operation == "")
                {
                    lefеElement += s;
                }
                else
                {
                    rightElement += s;
                }
            }

            else
            {
                if (s == "=")
                {
                    Update_RightOp();
                    textBlock.Text += rightElement;
                    operation = "";
                }
                else if (s == "CLEAR")
                {
                    lefеElement = "";
                    rightElement = "";
                    operation = "";
                    textBlock.Text = "";
                }
                else
                {
                    if (rightElement != "")
                    {
                        Update_RightOp();
                        lefеElement = rightElement;
                        rightElement = "";
                    }
                    operation = s;
                }
            }
        }

        private void Update_RightOp()
        {
            try
            {
               int num1 = Int32.Parse(lefеElement);
               int num2 = Int32.Parse(rightElement);

            switch (operation)
                {
                case "+":rightElement = (num1 + num2).ToString();break;
                case "-":rightElement = (num1 - num2).ToString();break;
                case "*":rightElement = (num1 * num2).ToString();break;
                case "/":rightElement = (num1 / num2).ToString();break;
                }
             }

            catch (Exception)
            {
                MessageBox.Show("Задайте верные значения!");
                lefеElement = "";
                rightElement = "";
                operation = "";
                textBlock.Text = "";
            }

        }
    }
}
