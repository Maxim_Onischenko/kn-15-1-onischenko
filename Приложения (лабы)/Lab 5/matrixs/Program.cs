﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrixs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размер квадратных матриц");
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] arr1 = new int[n, n];
            int[,] arr2 = new int[n, n];
            Random ran = new Random();
            Console.WriteLine("Первая матрица");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    arr1[i, j] = ran.Next(1, 15);
                    Console.Write("{0}\t", arr1[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Вторая матрица");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                   arr2[i, j] = ran.Next(1, 15);
                    Console.Write("{0}\t", arr2[i, j]);
                }
                Console.WriteLine();
            }
            int[,] arr3 = new int[n, n];
            Console.WriteLine("Сумма матриц");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    arr3[i, j] = arr1[i, j] + arr2[i, j];
                    Console.Write("{0}\t", arr3[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("Разница матриц");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    arr3[i, j] = arr1[i, j] - arr2[i, j];
                    Console.Write("{0}\t", arr3[i, j]);
                }
                Console.WriteLine();
            }
            Array.Clear(arr3, 0, n);

            Console.WriteLine("Умножение матриц");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    for (int k = 0; k < n; k++)
                    {
                        arr3[i, j] += arr1[i, k] * arr2[k, j];
                    }
                }
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write("{0} ", arr3[i, j]);
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
